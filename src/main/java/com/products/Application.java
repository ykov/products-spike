package com.products;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties
@SpringBootApplication
public class Application {

    @Value("${spring.application.name:}")
    private String serviceName;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


}