package com.products.domain.model;

import com.products.domain.model.product.Price;

import java.util.Currency;
import java.util.Optional;

public interface PriceConverterAdapter {

    Optional<Price> convert (Price price, Currency currency);
}
