package com.products.domain.model.support;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.Optional;
import java.util.function.Predicate;

@EqualsAndHashCode
@ToString
public abstract class NonEmptyString {

    @Getter
    private final String value;

    public NonEmptyString(String value) {
        this.value = Optional.ofNullable(value)
                .filter(Predicate.not(String::isBlank))
                .orElseThrow(IllegalArgumentException::new);
    }

    public String value() {
        return value;
    }

}