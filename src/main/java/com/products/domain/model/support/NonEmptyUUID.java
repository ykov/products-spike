package com.products.domain.model.support;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.Optional;
import java.util.UUID;
import java.util.function.Predicate;

@EqualsAndHashCode
@ToString
public abstract class NonEmptyUUID {

    @Getter
    private final UUID uuid;

    public NonEmptyUUID(String value) {
        this.uuid = Optional.ofNullable(value)
                .filter(Predicate.not(String::isBlank))
                .map(UUID::fromString)
                .orElseThrow(IllegalArgumentException::new);
    }

    public NonEmptyUUID(UUID value) {
        this.uuid = Optional.ofNullable(value)
                .orElseThrow(IllegalArgumentException::new);
    }

    public UUID uuid() {
        return uuid;
    }

    public String uuidAsString() {
        return uuid.toString();
    }

}