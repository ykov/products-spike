package com.products.domain.model.product;

import com.products.domain.model.support.NonEmptyUUID;

import java.util.UUID;

public class ProductId extends NonEmptyUUID {

    public ProductId(String value) {
        super(value);
    }

    public ProductId(UUID value) {
        super(value);
    }

    public static ProductId create() {
        return new ProductId(UUID.randomUUID());
    }


}
