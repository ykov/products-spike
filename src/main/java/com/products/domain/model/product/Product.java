package com.products.domain.model.product;

import com.products.domain.model.category.CategoryId;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;

import java.util.List;
import java.util.Optional;


@EqualsAndHashCode()
@ToString
public class Product {

    private final ProductId productId;
    private final List<CategoryId> categories;
    //        private final Manufacturer manufacturer;
    private final ProductName name;
    //        private final ProductSize size;
//        private final ProductColour colourr;
//        private final Barcode barcode;
    private final Price originalPrice;
    private Price convertedPrice;


    public Product(@NonNull ProductId productId, List<CategoryId> categories, @NonNull ProductName name, Price originalPrice, Price convertedPrice) {
        this.productId = productId;
        this.categories = Optional.ofNullable(categories).map(List::copyOf).orElse(List.of());
        this.name = name;
        this.originalPrice = originalPrice;
        this.convertedPrice = convertedPrice;
    }

    public ProductId productId() {
        return productId;
    }

    public ProductName productName() {
        return name;
    }

    public List<CategoryId> categories() {
        return categories;
    }

    public Optional<Price> originalPrice() {
        return Optional.ofNullable(originalPrice);
    }

    public Optional<Price> convertedPrice() {
        return Optional.ofNullable(convertedPrice);
    }
}
