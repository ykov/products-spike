package com.products.domain.model.product;

import com.products.domain.model.support.NonEmptyString;

public class ProductName extends NonEmptyString {

    public ProductName(String value) {
        super(value);
    }
}
