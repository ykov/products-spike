package com.products.domain.model.product;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface ProductRepository {

    Product save(Product product);

    void delete(ProductId productId);

    Optional<Product> find(ProductId productId);

    List<Product> findAll();
}
