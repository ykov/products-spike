package com.products.domain.model.product;

import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;

import java.util.Currency;

@EqualsAndHashCode
@ToString
public class Price {
    private final Currency currency;
    private final long amount;

    public Price(long amount, @NonNull Currency currency) {
        this.currency = currency;
        this.amount = amount;
    }

    public long amount(){
        return amount;
    }

    public Currency currency() {
        return currency;
    }
}
