package com.products.domain.model.category;

import com.products.domain.model.support.NonEmptyUUID;

import java.util.UUID;

public class CategoryId extends NonEmptyUUID {
    public CategoryId(String value) {
        super(value);
    }

    public CategoryId(UUID value) {
        super(value);
    }

    public static CategoryId create() {
        return new CategoryId(UUID.randomUUID());
    }
}
