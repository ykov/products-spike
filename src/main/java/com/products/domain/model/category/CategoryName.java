package com.products.domain.model.category;

import com.products.domain.model.support.NonEmptyString;

public class CategoryName extends NonEmptyString {
    public CategoryName(String value) {
        super(value);
    }
}
