package com.products.domain.model.category;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode()
@ToString
public class Category {
    private final CategoryId categoryId;
    private final CategoryId parent;
    private final CategoryName categoryName;

    public Category(CategoryId categoryId, CategoryId parent, CategoryName categoryName) {
        this.categoryId = categoryId;
        this.parent = parent;
        this.categoryName = categoryName;
    }

    public CategoryId categoryId() {
        return categoryId;
    }

    public CategoryId parent() {
        return parent;
    }

    public CategoryName categoryName() {
        return categoryName;
    }
}
