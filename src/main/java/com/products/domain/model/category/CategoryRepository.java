package com.products.domain.model.category;

import java.util.Optional;

public interface CategoryRepository {
    Category save(Category category);

    void delete(CategoryId categoryId);

    Optional<Category> find(CategoryId categoryId);
}
