package com.products.application;

import com.products.domain.model.PriceConverterAdapter;
import com.products.domain.model.product.Price;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Optional;

@Service
public class PriceConverterService {

    public final Currency defaultCurrency = Currency.getInstance("EUR");
    private final PriceConverterAdapter priceConverterAdapter;

    public PriceConverterService(PriceConverterAdapter priceConverterAdapter) {
        this.priceConverterAdapter = priceConverterAdapter;
    }

    public Optional<Price> convert(PriceJson priceJson) {
        return Optional.ofNullable(priceJson)
                .map(this::convertPrice);
    }

    public PriceJson convert(Price price) {
        return PriceJson.builder()
                .amount(toBigDecimal(price.amount()))
                .currency(price.currency())
                .build();
    }

    private BigDecimal toBigDecimal(long amount) {
        return new BigDecimal(amount).movePointLeft(2);
    }

    public Optional<Price> convertToDefaultCurrency(Price price) {
        if (price != null && !defaultCurrency.equals(price.currency())) {
            return priceConverterAdapter.convert(price, defaultCurrency);
        }
        return Optional.ofNullable(price);
    }

    private Price convertPrice(PriceJson priceJson) {
        return new Price(toLong(priceJson.amount()), priceJson.currency());
    }

    private long toLong (BigDecimal amount) {
        return Optional.ofNullable(amount)
                .map(bg -> bg.movePointRight(2))
                .map(BigDecimal::longValue)
                .orElseThrow(IllegalArgumentException::new);
    }
}
