package com.products.application;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CategoryResponseJson {
    @JsonProperty
    private String id;
    @JsonProperty
    private String parent;
    @JsonProperty
    private String name;
    @JsonProperty
    private String error;
}
