package com.products.application;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductResponseJson extends ProductRequestJson {
    @JsonProperty
    private String productId;
    @JsonProperty
    private String name;
    @JsonProperty
    private List<String> categories;
    @JsonProperty
    private PriceJson originalPrice;
    @JsonProperty
    private PriceJson convertedPrice;
    @JsonProperty
    private String error;

}
