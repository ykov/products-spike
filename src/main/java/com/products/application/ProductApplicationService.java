package com.products.application;

import com.products.domain.model.category.CategoryId;
import com.products.domain.model.product.Product;
import com.products.domain.model.product.ProductId;
import com.products.domain.model.product.ProductName;
import com.products.domain.model.product.ProductRepository;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class ProductApplicationService {

    private final ProductRepository productRepository;
    private final PriceConverterService priceConverterService;

    public ProductApplicationService(ProductRepository productRepository, PriceConverterService priceConverterService) {
        this.productRepository = productRepository;
        this.priceConverterService = priceConverterService;
    }

    public ProductResponseJson create(@NonNull ProductRequestJson json) {
        var product = initProduct(null, json);
        return saveProduct(product);
    }

    public ProductResponseJson update(@NonNull String id, @NonNull ProductRequestJson json) {
        var product = initProduct(id, json);
        return saveProduct(product);
    }


    public ProductResponseJson delete(@NonNull String id) {
        var productId = new ProductId(id);
        productRepository.delete(productId);
        return ProductResponseJson.builder().productId(id).build();
    }

    public ProductResponseJson find(@NonNull String id) {
        var productId = new ProductId(id);
        var found = productRepository.find(productId);
        return found.map(this::buildResponse).orElseThrow(IllegalArgumentException::new);
    }

    public List<ProductResponseJson> findAll() {
        return productRepository.findAll().stream()
                .map(this::buildResponse)
                .collect(Collectors.toList());
    }

    private ProductResponseJson saveProduct(Product product) {
        var saved = productRepository.save(product);
        return buildResponse(saved);
    }

    private ProductResponseJson buildResponse(Product product) {
        return ProductResponseJson.builder()
                .productId(product.productId().uuidAsString())
                .name(product.productName().value())
                .categories(product.categories().stream().map(CategoryId::uuidAsString).collect(Collectors.toList()))
                .originalPrice(product.originalPrice().map(priceConverterService::convert).orElse(null))
                .convertedPrice(product.convertedPrice().map(priceConverterService::convert).orElse(null))
                .build();
    }

    private Product initProduct(String id, ProductRequestJson json) {
        var price = priceConverterService.convert(json.originalPrice()).orElse(null);
        return new Product(
                Optional.ofNullable(id).filter(Predicate.not(String::isBlank)).map(ProductId::new).orElse(ProductId.create()),
                initCategories(json.categories()),
                new ProductName(json.name()),
                price,
                priceConverterService.convertToDefaultCurrency(price).orElse(null)
        );
    }

    private List<CategoryId> initCategories(List<String> cats) {
        return Optional.ofNullable(cats).stream()
                .flatMap(Collection::stream)
                .map(CategoryId::new)
                .collect(Collectors.toList());
    }
}
