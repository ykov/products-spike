package com.products.application;

import com.products.domain.model.category.Category;
import com.products.domain.model.category.CategoryId;
import com.products.domain.model.category.CategoryName;
import com.products.domain.model.category.CategoryRepository;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.function.Predicate;

@Service
public class CategoryApplicationService {

    private final CategoryRepository categoryRepository;

    public CategoryApplicationService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public CategoryResponseJson create(@NonNull CategoryRequestJson json) {
        var category = initCategory(null, json);
        return saveCategory(category);
    }

    public CategoryResponseJson update(@NonNull String id, @NonNull CategoryRequestJson json) {
        var category = initCategory(id, json);
        return saveCategory(category);
    }


    public CategoryResponseJson delete(@NonNull String id) {
        var categoryId = new CategoryId(id);
        categoryRepository.delete(categoryId);
        return CategoryResponseJson.builder().id(id).build();
    }

    public CategoryResponseJson find(@NonNull String id) {
        var categoryId = new CategoryId(id);
        var found = categoryRepository.find(categoryId);
        return found.map(this::buildResponse).orElseThrow(IllegalArgumentException::new);
    }

    private CategoryResponseJson saveCategory(Category category) {
        var saved = categoryRepository.save(category);
        return buildResponse(saved);
    }

    private CategoryResponseJson buildResponse(Category category) {
        return CategoryResponseJson.builder()
                .id(category.categoryId().uuidAsString())
                .parent(category.parent().uuidAsString())
                .name(category.categoryName().value())
                .build();
    }

    private Category initCategory(String id, CategoryRequestJson json) {
        return new Category(
                initCategoryId(id)
                        .orElse(CategoryId.create()),
                initCategoryId(json.parent())
                        .orElse(null),
                new CategoryName(json.name())
        );
    }

    private Optional<CategoryId> initCategoryId(String id) {
        return Optional.ofNullable(id)
                .filter(Predicate.not(String::isBlank))
                .map(CategoryId::new);
    }

}
