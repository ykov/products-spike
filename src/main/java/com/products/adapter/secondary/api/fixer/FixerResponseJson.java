package com.products.adapter.secondary.api.fixer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;
import java.util.Map;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class FixerResponseJson {
    @JsonProperty
    private boolean success;
    @JsonProperty
    private String timestamp;
    @JsonProperty
    private Currency base;
    @JsonProperty
    private LocalDate date;
    @JsonProperty
    private Map<String, BigDecimal> rates;

}
