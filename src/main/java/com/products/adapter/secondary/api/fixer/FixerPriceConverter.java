package com.products.adapter.secondary.api.fixer;

import com.products.domain.model.PriceConverterAdapter;
import com.products.domain.model.product.Price;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.Optional;

import static java.lang.String.join;

@Slf4j
@Component
public class FixerPriceConverter implements PriceConverterAdapter {

    private final FixerConfig config;

    public FixerPriceConverter(FixerConfig config) {
        this.config = config;
    }

    @Override
    public Optional<Price> convert(@NonNull Price price, @NonNull Currency currency) {
        var rate = getRateForCurrency(price.currency());
        if (rate.isPresent()) {
            LOG.info("Found rate for {} = {}", price.currency(), rate);
            var amount = calculate(price.amount(), rate.get());
            return Optional.of(new Price(amount, currency));
        }
        return Optional.empty();
    }

    private long calculate(long amount, BigDecimal rate) {
        return new BigDecimal(amount).movePointLeft(2)
                .divide(rate, 2, RoundingMode.HALF_UP)
                .movePointRight(2).longValue();
    }


    private Optional<BigDecimal> getRateForCurrency(Currency currency) {
        var uri = getLatestRequest();
        var restTemplate = new RestTemplate();
        var response = restTemplate.getForObject(uri, FixerResponseJson.class);
        if (response != null && response.success()) {
            return Optional.ofNullable(response.rates().get(currency.toString()));
        } else {
            LOG.error("Didnt get successful response from fixer.io");
            return Optional.empty();
        }

    }

    private String getLatestRequest() {
        return join("",
                config.getEndpoint(),
                "latest?",
                "access_key=",
                config.getApiKey()
        );
    }
}
