package com.products.adapter.secondary.database;

import com.products.domain.model.category.Category;
import com.products.domain.model.category.CategoryId;
import com.products.domain.model.category.CategoryName;
import com.products.domain.model.category.CategoryRepository;
import lombok.NonNull;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class DatabaseCategoryRepository implements CategoryRepository {
    private final JpaCategoryRepository jpaCategoryRepository;

    public DatabaseCategoryRepository(JpaCategoryRepository jpaCategoryRepository) {
        this.jpaCategoryRepository = jpaCategoryRepository;
    }

    @Override
    public Category save(@NonNull Category category) {
        var jpa = convert(category);
        var saved = jpaCategoryRepository.save(jpa);
        return convert(saved);
    }

    @Override
    public void delete(@NonNull CategoryId categoryId) {
        jpaCategoryRepository.deleteById(categoryId.uuid());
    }

    @Override
    public Optional<Category> find(CategoryId categoryId) {
        var found = jpaCategoryRepository.findById(categoryId.uuid());
        return found.map(this::convert);
    }

    private Category convert(CategoryJpa categoryJpa) {
        return new Category(
                new CategoryId(categoryJpa.uuid()),
                new CategoryId(categoryJpa.parent()),
                new CategoryName(categoryJpa.name())
        );
    }

    private CategoryJpa convert(Category category) {
        return CategoryJpa.builder()
                .uuid(category.categoryId().uuid())
                .parent(category.parent().uuid())
                .name(category.categoryName().value())
                .build();
    }
}
