package com.products.adapter.secondary.database;

import com.products.domain.model.category.CategoryId;
import com.products.domain.model.product.Price;
import com.products.domain.model.product.Product;
import com.products.domain.model.product.ProductId;
import com.products.domain.model.product.ProductName;
import com.products.domain.model.product.ProductRepository;
import lombok.NonNull;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.Collection;
import java.util.Currency;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class DatabaseProductRepository implements ProductRepository {

    private final JpaProductRepository jpaProductRepository;

    public DatabaseProductRepository(JpaProductRepository jpaProductRepository) {
        this.jpaProductRepository = jpaProductRepository;
    }

    @Override
    public Product save(@NonNull Product product) {
        var jpa = convert(product);
        var saved = jpaProductRepository.save(jpa);
        return convert(saved);
    }

    @Override
    public void delete(@NonNull ProductId productId) {
        jpaProductRepository.deleteById(productId.uuid());
    }

    @Override
    public Optional<Product> find(ProductId productId) {
        var found = jpaProductRepository.findById(productId.uuid());
        return found.map(this::convert);
    }

    @Override
    public List<Product> findAll() {
        return jpaProductRepository.findAll().stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

    private Product convert(ProductJpa productJpa) {
        return new Product(
                new ProductId(productJpa.uuid()),
                convert(productJpa.categories()),
                new ProductName(productJpa.name()),
                convertPrice(productJpa.originalPrice(), productJpa.originalPriceCurrency()),
                convertPrice(productJpa.convertedPrice(), productJpa.convertedPriceCurrency())
        );
    }

    private Price convertPrice(Long amount, Currency currency) {
        if (amount != null && currency != null) {
            return new Price(amount, currency);
        } else {
            return null;
        }
    }

    private ProductJpa convert(@NonNull Product product) {
        return ProductJpa.builder()
                .uuid(product.productId().uuid())
                .name(product.productName().value())
                .categories(convert(product.categories()))
                .originalPrice(product.originalPrice().map(Price::amount).orElse(null))
                .originalPriceCurrency(product.originalPrice().map(Price::currency).orElse(null))
                .convertedPrice(product.convertedPrice().map(Price::amount).orElse(null))
                .convertedPriceCurrency(product.convertedPrice().map(Price::currency).orElse(null))
                .build();
    }

    private String[] convert(List<CategoryId> categories) {
        return Optional.ofNullable(categories).stream()
                .flatMap(Collection::stream)
                .map(CategoryId::uuidAsString)
                .toArray(String[]::new);
    }

    private List<CategoryId> convert(String[] categories) {
        return Optional.ofNullable(categories)
                .map(Arrays::asList).stream()
                .flatMap(Collection::stream)
                .map(CategoryId::new)
                .collect(Collectors.toList());
    }

}
