package com.products.adapter.secondary.database;

import com.vladmihalcea.hibernate.type.array.IntArrayType;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.OffsetDateTime;
import java.util.Currency;
import java.util.UUID;

@Entity
@Table(name = "products")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@TypeDefs(@TypeDef(name = "string-array", typeClass = StringArrayType.class))
public class ProductJpa {
    @Id
    private UUID uuid;
    @Column
    private String name;
    @Column(columnDefinition = "text[]")
    @Type(type = "string-array" )
    private String [] categories;
    @Column
    private Long originalPrice;
    @Column
    private Currency originalPriceCurrency;
    @Column
    private Long convertedPrice;
    @Column
    private Currency convertedPriceCurrency;
    @Column(updatable = false)
    @CreatedDate
    private OffsetDateTime created;
    @Column
    @LastModifiedDate
    private OffsetDateTime modified;
}
