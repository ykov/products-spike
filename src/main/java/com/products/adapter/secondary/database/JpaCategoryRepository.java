package com.products.adapter.secondary.database;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface JpaCategoryRepository extends JpaRepository<CategoryJpa, UUID> {
}
