package com.products.adapter.primary.api;


import com.products.application.ProductApplicationService;
import com.products.application.ProductRequestJson;
import com.products.application.ProductResponseJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value="Product Management ")
@Slf4j
@RestController
@RequestMapping(ProductRestController.PATH)
public class ProductRestController {

    public static final String PATH = "/products";
    private final ProductApplicationService productApplicationService;

    public ProductRestController(ProductApplicationService productApplicationService) {
        this.productApplicationService = productApplicationService;
    }


    @ApiOperation(value = "Create a new poduct", response = ProductResponseJson.class)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ProductResponseJson> create(@RequestBody ProductRequestJson json) {
        try {
            var response = productApplicationService.create(json);
            LOG.info("Created product with id= {}", response.productId());
            return ResponseEntity.ok(response);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(ProductResponseJson.builder().error(e.getMessage()).build());
        } catch (Exception e) {
            LOG.error("Error during creation of the product", e);
            throw new ResponseException("Internal error");
        }
    }

    @ApiOperation(value = "Modifiy the existing poduct", response = ProductResponseJson.class)
    @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ProductResponseJson> update(@RequestBody ProductRequestJson json, @PathVariable("id") @NonNull String id) {
        try {
            var response = productApplicationService.update(id, json);
            LOG.info("Updated product with id= {}", response.productId());
            return ResponseEntity.ok(response);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(ProductResponseJson.builder().error(e.getMessage()).build());
        }catch (Exception e) {
            LOG.error("Error during updating of the product", e);
            throw new ResponseException("Internal error");
        }
    }

    @ApiOperation(value = "Delete the existing product", response = ProductResponseJson.class)
    @DeleteMapping(path = "/{id}")
    @ResponseBody
    public ResponseEntity<ProductResponseJson> delete(@PathVariable("id") @NonNull String id) {
        try {
            var response = productApplicationService.delete(id);
            LOG.info("Deleted product with id= {}", response.productId());
            return ResponseEntity.ok(response);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(ProductResponseJson.builder().error(e.getMessage()).build());
        }catch (Exception e) {
            LOG.error("Error during deletion of the product", e);
            throw new ResponseException("Internal error");
        }
    }

    @ApiOperation(value = "Find the existing product", response = ProductResponseJson.class)
    @GetMapping(path = "/{id}")
    @ResponseBody
    public ResponseEntity<ProductResponseJson> find(@PathVariable("id") @NonNull String id) {
        try {
            var response = productApplicationService.find(id);
            LOG.info("Found product with id= {}", response.productId());
            return ResponseEntity.ok(response);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(ProductResponseJson.builder().error("no product found").build());
        } catch (Exception e) {
            LOG.error("Error during finding of the product", e);
            throw new ResponseException("Internal error");
        }
    }

    @ApiOperation(value = "Find all existing product", response = ProductResponseJson.class)
    @GetMapping()
    @ResponseBody
    public ResponseEntity find() {
        try {
            var response = productApplicationService.findAll();
            LOG.info("Found product with size= {}", response.size());
            return ResponseEntity.ok(response);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(ProductResponseJson.builder().error("no product found").build());
        } catch (Exception e) {
            LOG.error("Error during finding of the product", e);
            throw new ResponseException("Internal error");
        }
    }
}
