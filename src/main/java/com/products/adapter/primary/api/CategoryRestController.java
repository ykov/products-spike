package com.products.adapter.primary.api;


import com.products.application.CategoryApplicationService;
import com.products.application.CategoryRequestJson;
import com.products.application.CategoryResponseJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Api(value="Category Management ")
@Slf4j
@RestController
@RequestMapping(CategoryRestController.PATH)
public class CategoryRestController {

    static final String PATH = "/products/categories";
    private final CategoryApplicationService categoryApplicationService;

    public CategoryRestController(CategoryApplicationService categoryApplicationService) {
        this.categoryApplicationService = categoryApplicationService;
    }

    @ApiOperation(value = "Create a new category", response = CategoryResponseJson.class)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<CategoryResponseJson> create(@RequestBody CategoryRequestJson json) {
        try {
            var response = categoryApplicationService.create(json);
            LOG.info("Created category with id= {}", response.id());
            return ResponseEntity.ok(response);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(CategoryResponseJson.builder().error(e.getMessage()).build());
        } catch (Exception e) {
            LOG.error("Error during creation of the category", e);
            throw new ResponseException("Internal error");
        }    }

    @ApiOperation(value = "Modifiy the existing category", response = CategoryResponseJson.class)
    @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<CategoryResponseJson> update(@RequestBody CategoryRequestJson json, @PathVariable("id") @NonNull String id) {
        try {
            var response = categoryApplicationService.update(id, json);
            LOG.info("Updated category with id= {}", response.id());
            return ResponseEntity.ok(response);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(CategoryResponseJson.builder().error(e.getMessage()).build());
        }catch (Exception e) {
            LOG.error("Error during updating of the category", e);
            throw new ResponseException("Internal error");
        }
    }

    @ApiOperation(value = "Delete the existing category", response = CategoryResponseJson.class)
    @DeleteMapping(path = "/{id}")
    @ResponseBody
    public ResponseEntity<CategoryResponseJson> delete(@PathVariable("id") @NonNull String id) {
        try {
            var response = categoryApplicationService.delete(id);
            LOG.info("Deleted category with id= {}", response.id());
            return ResponseEntity.ok(response);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(CategoryResponseJson.builder().error(e.getMessage()).build());
        }catch (Exception e) {
            LOG.error("Error during deletion of the category", e);
            throw new ResponseException("Internal error");
        }
    }

    @ApiOperation(value = "Find the existing category", response = CategoryResponseJson.class)
    @GetMapping(path = "/{id}")
    @ResponseBody
    public ResponseEntity<CategoryResponseJson> find(@PathVariable("id") @NonNull String id) {
        try {
            var response = categoryApplicationService.find(id);
            LOG.info("Found category with id= {}", response.id());
            return ResponseEntity.ok(response);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(CategoryResponseJson.builder().error("no category found").build());
        } catch (Exception e) {
            LOG.error("Error during finding of the category", e);
            throw new ResponseException("Internal error");
        }
    }
}
