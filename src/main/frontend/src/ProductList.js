import React, { Component } from 'react';
import { Container, Table } from 'reactstrap';
import { Link } from 'react-router-dom';

class ProductList extends Component {

    constructor(props) {
        super(props);
        this.state = {products: [], isLoading: true};
    }

    componentDidMount() {
        this.setState({isLoading: true});

        fetch('products-spike/products')
            .then(response => response.json())
            .then(data => this.setState({products: data, isLoading: false}));
    }

    render() {
        const {products, isLoading} = this.state;

        if (isLoading) {
            return <p>Loading...</p>;
        }

        const productList = products.map(product => {
            return <tr key={product.productId}>
                <td style={{whiteSpace: 'nowrap'}}>{product.name}</td>
                <td>{product.categories.map(category => <div>{category}</div>)}</td>
                <td style={{whiteSpace: 'nowrap'}}>{product.originalPrice.amount}</td>
                <td style={{whiteSpace: 'nowrap'}}>{product.originalPrice.currency}</td>
            </tr>
        });

        return (
            <div>
                <Container fluid>
                    <Table className="mt-4">
                        <thead>
                        <tr>
                            <th width="20%">Name</th>
                            <th width="20%">Categories</th>
                            <th width="10%">Price</th>
                            <th width="10%">Currency</th>
                        </tr>
                        </thead>
                        <tbody>
                        {productList}
                        </tbody>
                    </Table>
                </Container>
            </div>
        );
    }
}

export default ProductList;