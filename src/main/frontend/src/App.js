import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ProductList from './ProductList';

class App extends Component {
  render() {
    return (
        <Router>
          <Switch>
            <Route path='/products' exact={true} component={ProductList}/>
          </Switch>
        </Router>
    )
  }
}

export default App;