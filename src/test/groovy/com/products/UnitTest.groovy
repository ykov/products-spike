package com.products

import com.github.tomakehurst.wiremock.junit.WireMockRule
import org.junit.Rule
import spock.lang.Specification

abstract class UnitTest extends Specification {

  @Rule
  WireMockRule wiremock = new WireMockRule()

  def static EUR = Currency.getInstance("EUR")
  def static CHF = Currency.getInstance("CHF")
  def static GBP = Currency.getInstance("GBP")

  def cleanup() {
    wiremock.resetAll()
  }
}
