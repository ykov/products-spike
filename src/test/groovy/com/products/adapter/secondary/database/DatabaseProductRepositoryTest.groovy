package com.products.adapter.secondary.database

import com.products.Application
import com.products.UnitTest
import com.products.configuration.DatabaseConfiguration
import com.products.domain.model.category.CategoryId
import com.products.domain.model.product.ProductId
import com.products.domain.model.product.ProductName
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.dao.DataAccessException
import org.springframework.test.context.ContextConfiguration

import java.time.OffsetDateTime

import static com.products.ProductFixture.product

@DataJpaTest
@ContextConfiguration(classes = [Application.class, DatabaseConfiguration.class])
class DatabaseProductRepositoryTest extends UnitTest {

    @Autowired
    private JpaProductRepository jpaProductRepository

    private DatabaseProductRepository underTest

    def setup() {
        underTest = new DatabaseProductRepository(jpaProductRepository)
    }

    def "should save product"() {
        given:
        def name = "ProductName"
        def uuid = UUID.randomUUID()
        def product = product(productId: new ProductId(uuid), name: new ProductName(name))

        when:
        def result = underTest.save(product)

        then:
        result.productId() == new ProductId(uuid)
        result.productName() == new ProductName(name)

        and:
        def saved = jpaProductRepository.findAll()
        saved.size() == 1
        saved[0].uuid == uuid
        saved[0].name == name
        saved[0].originalPrice() == product.originalPrice().get().amount()
        saved[0].originalPriceCurrency() == product.originalPrice().get().currency()
        saved[0].convertedPrice() == product.originalPrice().get().amount()
        saved[0].convertedPriceCurrency() == product.originalPrice().get().currency()
    }

    def "should save product with categories"() {
        given:
        def name = "ProductName"
        def uuid = UUID.randomUUID()
        def catone = UUID.randomUUID()
        def cattwo = UUID.randomUUID()
        def product = product(productId: new ProductId(uuid), name: new ProductName(name), categories: [new CategoryId(catone), new CategoryId(cattwo)])

        when:
        def result = underTest.save(product)

        then:
        result.productId() == new ProductId(uuid)
        result.productName() == new ProductName(name)

        and:
        def saved = jpaProductRepository.findAll()
        saved.size() == 1
        saved[0].uuid == uuid
        saved[0].name == name
        saved[0].categories() == [catone.toString(), cattwo.toString()]

    }

    def "should delete products"() {
        given:
        def name = "ProductName"
        def uuid = UUID.randomUUID()
        def product = ProductJpa.builder().uuid(uuid).name(name).created(OffsetDateTime.now()).modified(OffsetDateTime.now()).build()
        jpaProductRepository.save(product)

        when:
        underTest.delete(new ProductId(uuid))

        then:
        jpaProductRepository.findAll().size() == 0
    }

    def "should not delete empty products"() {
        when:
        underTest.delete(new ProductId(UUID.randomUUID()))

        then:
        thrown DataAccessException
    }

    def "should find products"() {
        given:
        def name = "ProductName"
        def uuid = UUID.randomUUID()
        def productBuilder = ProductJpa.builder().uuid(uuid).name(name).created(OffsetDateTime.now()).modified(OffsetDateTime.now())
        jpaProductRepository.save(productBuilder.build())
        jpaProductRepository.save(productBuilder.uuid(UUID.randomUUID()).build())

        when:
        def result = underTest.find(new ProductId(uuid))

        then:
        result.isPresent()
        result.get().productName().value() == name
        result.get().productId().uuid() == uuid
    }

    def "should not find products"() {
        when:
        def result = underTest.find(new ProductId(UUID.randomUUID()))

        then:
        result.isEmpty()
    }

}