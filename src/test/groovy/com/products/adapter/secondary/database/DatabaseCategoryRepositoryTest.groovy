package com.products.adapter.secondary.database

import com.products.Application
import com.products.UnitTest
import com.products.configuration.DatabaseConfiguration
import com.products.domain.model.category.CategoryId
import com.products.domain.model.category.CategoryName
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.dao.DataAccessException
import org.springframework.test.context.ContextConfiguration

import java.time.OffsetDateTime

import static com.products.CategoryFixture.category

@DataJpaTest
@ContextConfiguration(classes = [Application.class, DatabaseConfiguration.class])
class DatabaseCategoryRepositoryTest extends UnitTest {

    @Autowired
    private JpaCategoryRepository jpaCategoryRepository

    private DatabaseCategoryRepository underTest

    def setup() {
        underTest = new DatabaseCategoryRepository(jpaCategoryRepository)
    }

    def "should save category"() {
        given:
        def name = "CategoryName"
        def uuid = UUID.randomUUID()
        def parent = UUID.randomUUID()
        def category = category(categoryId: new CategoryId(uuid), categoryName: new CategoryName(name), parent: new CategoryId(parent))

        when:
        def result = underTest.save(category)

        then:
        result.categoryId() == new CategoryId(uuid)
        result.parent() == new CategoryId(parent)
        result.categoryName() == new CategoryName(name)

        and:
        def saved = jpaCategoryRepository.findAll()
        saved.size() == 1
        saved[0].uuid == uuid
        saved[0].name == name
        saved[0].parent == parent
    }

    def "should delete category"() {
        given:
        def name = "CategoryName"
        def uuid = UUID.randomUUID()
        def parent = UUID.randomUUID()
        def product = CategoryJpa.builder().uuid(uuid).name(name).parent(parent).created(OffsetDateTime.now()).modified(OffsetDateTime.now()).build()
        jpaCategoryRepository.save(product)

        when:
        underTest.delete(new CategoryId(uuid))

        then:
        jpaCategoryRepository.findAll().size() == 0
    }

    def "should not delete empty categories"() {
        when:
        underTest.delete(new CategoryId(UUID.randomUUID()))

        then:
        thrown DataAccessException
    }

    def "should find category"() {
        given:
        def name = "CategoryName"
        def uuid = UUID.randomUUID()
        def parent = UUID.randomUUID()
        def productBuilder = CategoryJpa.builder().uuid(uuid).name(name).parent(parent).created(OffsetDateTime.now()).modified(OffsetDateTime.now())
        jpaCategoryRepository.save(productBuilder.build())
        jpaCategoryRepository.save(productBuilder.uuid(UUID.randomUUID()).build())

        when:
        def result = underTest.find(new CategoryId(uuid))

        then:
        result.isPresent()
        result.get().categoryName().value() == name
        result.get().categoryId().uuid() == uuid
        result.get().parent().uuid() == parent
    }

    def "should not find categories"() {
        when:
        def result = underTest.find(new CategoryId(UUID.randomUUID()))

        then:
        result.isEmpty()
    }

}