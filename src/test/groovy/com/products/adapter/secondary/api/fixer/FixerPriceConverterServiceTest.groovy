package com.products.adapter.secondary.api.fixer

import com.products.UnitTest
import com.products.domain.model.product.Price
import spock.lang.Unroll

import static com.github.tomakehurst.wiremock.client.WireMock.*

class FixerPriceConverterServiceTest extends UnitTest {

    def uri = "http://localhost:8080/"
    def apiKey = "apiKey"
    def config = Mock(FixerConfig) {
        getEndpoint() >> uri
        getApiKey() >> apiKey
    }
    FixerPriceConverter underTest = new FixerPriceConverter(config)

    def setup() {
        wiremock.stubFor(get(urlPathMatching("/latest"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; Charset=UTF-8")
                        .withBody(sucessResponse())))
    }

    @Unroll
    def "should convert price #price"() {

        when:
        def result = underTest.convert(price, currency)

        then:
        result.isPresent()
        result.get().amount() == expectedAmount
        result.get().currency() == expectedCurrency

        where:
        price                | currency | expectedAmount | expectedCurrency
        new Price(1245, GBP) | EUR      | 1479           | EUR
        new Price(0, GBP)    | EUR      | 0              | EUR
        new Price(668, CHF)  | EUR      | 609           | EUR


    }

    def sucessResponse() {
        return """
        {
            "success":true,
            "timestamp":1575749045,
            "base":"EUR",
            "date":"2019-12-07",
            "rates": {
                "GBP" : 0.84161,
                "CHF" : 1.096017
            }
        }"""
    }
}
