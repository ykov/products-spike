package com.products.adapter.primary.api

import com.products.UnitTest
import com.products.application.CategoryApplicationService
import com.products.domain.model.category.CategoryRepository
import groovy.json.JsonSlurper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

import static com.products.CategoryFixture.categoryJson
import static com.products.CategoryFixture.categoryResponseJson
import static org.mockito.ArgumentMatchers.any
import static org.mockito.ArgumentMatchers.eq
import static org.mockito.Mockito.when
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put

@WebMvcTest(value = CategoryRestController)
class CategoryRestControllerTest extends UnitTest {

    @Autowired
    MockMvc mockMvc
    @Autowired
    CategoryRestController underTest
    @MockBean
    CategoryApplicationService categoryApplicationService
    @MockBean
    CategoryRepository categoryRepository

    def "should create new category"() {
        given:
        def cname = "test"
        def categoryid = "categoryid"
        def jsonBody = categoryJson(name: "${cname}")
        when(categoryApplicationService.create(any())).thenReturn(categoryResponseJson(id: categoryid, name: cname))

        when:
        def response = mockMvc.perform(post("/products/categories")
                .content(jsonBody)
                .contentType(MediaType.APPLICATION_JSON))

        then:
        def content = response.andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().response

        and:
        def jsonData = new JsonSlurper().parseText(content.contentAsString)
        jsonData.id == categoryid
        jsonData.name == cname
    }

    def "should update category"() {
        given:
        def cname = "test"
        def categoryid = "categoryid"
        def jsonBody = categoryJson(name: "${cname}")
        when(categoryApplicationService.update(eq(categoryid), any())).thenReturn(categoryResponseJson(id: categoryid, name: cname))

        when:
        def response = mockMvc.perform(put("/products/categories/${categoryid}")
                .content(jsonBody)
                .contentType(MediaType.APPLICATION_JSON))

        then:
        def content = response.andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().response

        and:
        def jsonData = new JsonSlurper().parseText(content.contentAsString)
        jsonData.id == categoryid
        jsonData.name == cname
    }

    def "should find category"() {
        given:
        def cname = "test"
        def categoryid = "categoryid"
        when(categoryApplicationService.find(eq(categoryid))).thenReturn(categoryResponseJson(id: categoryid, name: cname))

        when:
        def response = mockMvc.perform(get("/products/categories/${categoryid}"))

        then:
        def content = response.andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().response

        and:
        def jsonData = new JsonSlurper().parseText(content.contentAsString)
        jsonData.id == categoryid
        jsonData.name == cname
    }

    def "should delete category"() {
        given:
        def categoryid = "categoryid"
        when(categoryApplicationService.delete(eq(categoryid))).thenReturn(categoryResponseJson(id:categoryid, name:null))

        when:
        def response = mockMvc.perform(delete("/products/categories/${categoryid}"))

        then:
        def content = response.andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().response

        and:
        def jsonData = new JsonSlurper().parseText(content.contentAsString)
        jsonData.id == categoryid
    }

}
