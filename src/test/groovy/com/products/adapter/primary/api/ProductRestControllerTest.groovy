package com.products.adapter.primary.api


import com.products.UnitTest
import com.products.application.ProductApplicationService
import com.products.application.ProductResponseJson
import com.products.domain.model.product.ProductRepository
import groovy.json.JsonSlurper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

import static com.products.ProductFixture.productJson
import static com.products.ProductFixture.productResponseJson
import static org.mockito.ArgumentMatchers.eq
import static org.mockito.Mockito.when
import static org.mockito.ArgumentMatchers.any
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put

@WebMvcTest(value = ProductRestController)
class ProductRestControllerTest extends UnitTest {

    @Autowired
    MockMvc mockMvc
    @Autowired
    ProductRestController underTest
    @MockBean
    ProductApplicationService productApplicationService
    @MockBean
    ProductRepository productRepository

    def "should create new product"() {
        given:
        def pname = "test"
        def productid = "productid"
        def categories = ["cat1", "cat2"]
        def jsonBody = productJson(name: "${pname}", catetories: categories)
        when(productApplicationService.create(any())).thenReturn(productResponseJson(name:pname, productId: productid, categories:categories ))

        when:
        def response = mockMvc.perform(post("/products")
                .content(jsonBody)
                .contentType(MediaType.APPLICATION_JSON))

        then:
        def content = response.andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().response

        and:
        def jsonData = new JsonSlurper().parseText(content.contentAsString)
        jsonData.productId == productid
        jsonData.name == pname
        jsonData.categories[0] == categories[0]
        jsonData.categories[1] == categories[1]
    }

    def "should update product"() {
        given:
        def pname = "test"
        def productid = "productid"
        def jsonBody = productJson(name: "${pname}")
        when(productApplicationService.update(eq(productid), any())).thenReturn(
                ProductResponseJson.builder()
                        .productId(productid)
                        .name(pname)
                        .build())

        when:
        def response = mockMvc.perform(put("/products/${productid}")
                .content(jsonBody)
                .contentType(MediaType.APPLICATION_JSON))

        then:
        def content = response.andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().response

        and:
        def jsonData = new JsonSlurper().parseText(content.contentAsString)
        jsonData.productId == productid
        jsonData.name == pname
    }

    def "should find product"() {
        given:
        def pname = "test"
        def productid = "productid"
        when(productApplicationService.find(eq(productid))).thenReturn(
                ProductResponseJson.builder()
                        .productId(productid)
                        .name(pname)
                        .build())

        when:
        def response = mockMvc.perform(get("/products/${productid}"))

        then:
        def content = response.andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().response

        and:
        def jsonData = new JsonSlurper().parseText(content.contentAsString)
        jsonData.productId == productid
        jsonData.name == pname
    }

    def "should delete product"() {
        given:
        def productid = "productid"
        when(productApplicationService.delete(eq(productid))).thenReturn(
                ProductResponseJson.builder()
                        .productId(productid)
                        .build())

        when:
        def response = mockMvc.perform(delete("/products/${productid}"))

        then:
        def content = response.andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().response

        and:
        def jsonData = new JsonSlurper().parseText(content.contentAsString)
        jsonData.productId == productid
    }

}
