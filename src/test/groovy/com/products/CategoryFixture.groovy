package com.products

import com.products.application.CategoryRequestJson
import com.products.application.CategoryResponseJson
import com.products.domain.model.category.Category
import com.products.domain.model.category.CategoryId
import com.products.domain.model.category.CategoryName
import groovy.json.JsonOutput

class CategoryFixture {

    static Category category(Map data = [:]) {
        new Category(
                data.get("categoryId", new CategoryId(UUID.randomUUID())),
                data.get("parent", new CategoryId(UUID.randomUUID())),
                data.get("categoryName", new CategoryName("CategoryName")),
        )
    }

    static String categoryJson(Map data = [:]) {
        JsonOutput.toJson([
                name: data.get("name", "defaultName"),
        ])
    }

    static CategoryRequestJson categoryRequestJson(Map data = [:]) {
        new CategoryRequestJson(
                data.get('name', "CategoryName"),
                data.get('parent', UUID.randomUUID().toString()))
    }

    static CategoryResponseJson categoryResponseJson(Map data = [:]) {
        CategoryResponseJson.builder()
                .name(data.get('name', "CategoryName"))
                .id(data.get('id', UUID.randomUUID().toString()))
                .parent(data.get("parent", UUID.randomUUID().toString()))
                .build()
    }

}
