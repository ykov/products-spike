package com.products.application

import com.products.UnitTest
import com.products.domain.model.category.CategoryId
import com.products.domain.model.category.CategoryName
import com.products.domain.model.category.CategoryRepository
import com.products.domain.model.product.ProductId

import static com.products.CategoryFixture.category
import static com.products.CategoryFixture.categoryRequestJson

class CategoryApplicationServiceTest extends UnitTest {

    CategoryRepository categoryRepository = Mock()

    CategoryApplicationService underTest = new CategoryApplicationService(categoryRepository)

    def "should create category"() {
        given:
        def name = "DefaultName"
        def request = categoryRequestJson(name: name)
        def category = category(name: new CategoryName(name))

        when:
        def result = underTest.create(request)

        then:
        1 * categoryRepository.save(_) >> category

        and:
        result.id() == category.categoryId().uuidAsString()
        result.name() == category.categoryName().value()
        result.parent() == category.parent().uuidAsString()
    }

    def "should update category"() {
        given:
        def id = UUID.randomUUID().toString()
        def name = "DefaultName"
        def parent = UUID.randomUUID().toString()
        def request = categoryRequestJson(name: name, parent: parent)
        def category = category(categoryName: new CategoryName(name), categoryId: new CategoryId(id), parent: new CategoryId(parent))

        when:
        def result = underTest.update(id, request)

        then:
        1 * categoryRepository.save(category) >> category

        and:
        result.id() == category.categoryId().uuidAsString()
        result.name() == category.categoryName().value()
        result.parent() == category.parent().uuidAsString()
    }

    def "should delete category"() {
        given:
        def id = UUID.randomUUID().toString()

        when:
        underTest.delete(id)

        then:
        1 * categoryRepository.delete(_)
    }

    def "should find category"() {
        given:
        def id = UUID.randomUUID().toString()
        def name = "DefaultName"
        def category = category(name: new CategoryName(name), categoryId: new CategoryId(id))

        when:
        def result = underTest.find(id)

        then:
        1 * categoryRepository.find(new ProductId(id)) >> Optional.of(category)

        and:
        result.id() == category.categoryId().uuidAsString()
        result.name() == category.categoryName().value()
    }

    def "should not find category"() {
        given:
        def id = UUID.randomUUID().toString()

        when:
        underTest.find(id)

        then:
        1 * categoryRepository.find(new CategoryId(id)) >> Optional.empty()

        and:
        thrown IllegalArgumentException
    }
}
