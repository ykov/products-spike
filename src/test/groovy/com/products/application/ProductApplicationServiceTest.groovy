package com.products.application

import com.products.UnitTest
import com.products.domain.model.product.Price
import com.products.domain.model.product.ProductId
import com.products.domain.model.product.ProductName
import com.products.domain.model.product.ProductRepository

import static com.products.ProductFixture.EUR
import static com.products.ProductFixture.product
import static com.products.ProductFixture.productRequestJson

class ProductApplicationServiceTest extends UnitTest {

    ProductRepository productRepository = Mock()
    PriceConverterService priceConverterService = Mock()

    ProductApplicationService underTest = new ProductApplicationService(productRepository, priceConverterService)

    def "should create product"() {
        given:
        def name = "DefaultName"
        def productRequest = productRequestJson(name: name)
        def product = product(name: new ProductName(name))
        def price  = new Price(0, EUR)
        def priceJson = PriceJson.builder().currency(EUR).amount(BigDecimal.ZERO).build()

        when:
        def result = underTest.create(productRequest)

        then:
        1 * productRepository.save(_) >> product
        1 * priceConverterService.convert(_ as PriceJson) >> Optional.of(price)
        2 * priceConverterService.convert(_ as Price) >> priceJson
        1 * priceConverterService.convertToDefaultCurrency(_) >> Optional.of(price)

        and:
        result.productId() == product.productId().uuidAsString()
        result.name() == product.productName().value()
    }

    def "should update product"() {
        given:
        def id = UUID.randomUUID().toString()
        def name = "DefaultName"
        def productRequest = productRequestJson(name: name)
        def product = product(name: new ProductName(name), productId: new ProductId(id))
        def price  = new Price(0, EUR)
        def priceJson = PriceJson.builder().currency(EUR).amount(BigDecimal.ZERO).build()

        when:
        def result = underTest.update(id, productRequest)

        then:
        1 * productRepository.save(_) >> product
        1 * priceConverterService.convert(_ as PriceJson) >> Optional.of(price)
        2 * priceConverterService.convert(_ as Price) >> priceJson
        1 * priceConverterService.convertToDefaultCurrency(_) >> Optional.of(price)

        and:
        result.productId() == product.productId().uuidAsString()
        result.name() == product.productName().value()
    }

    def "should delete product"() {
        given:
        def id = UUID.randomUUID().toString()

        when:
        underTest.delete(id)

        then:
        1 * productRepository.delete(_)
    }

    def "should find product"() {
        given:
        def id = UUID.randomUUID().toString()
        def name = "DefaultName"
        def product = product(name: new ProductName(name), productId: new ProductId(id))

        when:
        def result = underTest.find(id)

        then:
        1 * productRepository.find(new ProductId(id)) >> Optional.of(product)

        and:
        result.productId() == product.productId().uuidAsString()
        result.name() == product.productName().value()
    }

    def "should not find product"() {
        given:
        def id = UUID.randomUUID().toString()

        when:
        underTest.find(id)

        then:
        1 * productRepository.find(new ProductId(id)) >> Optional.empty()

        and:
        thrown IllegalArgumentException
    }
}
