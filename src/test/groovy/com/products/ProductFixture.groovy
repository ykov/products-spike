package com.products

import com.products.application.PriceJson
import com.products.application.ProductRequestJson
import com.products.application.ProductResponseJson
import com.products.domain.model.product.Price
import com.products.domain.model.product.Product
import com.products.domain.model.product.ProductId
import com.products.domain.model.product.ProductName
import groovy.json.JsonOutput

class ProductFixture {

    def static EUR = Currency.getInstance("EUR")

    static Product product(Map data = [:]) {
        new Product(
                data.get("productId", new ProductId(UUID.randomUUID())),
                data.get("categories", []),
                data.get("name", new ProductName("ProductName")),
                data.get("originalPrice", new Price(1099, EUR )),
                data.get("convertedPrice", new Price(1099, EUR )),
        )
    }

    static String productJson(Map data = [:]) {
        JsonOutput.toJson([
                name: data.get("name", "defaultName"),
                categories: data.get("categories", ["category"]),
        ])
    }

    static ProductRequestJson productRequestJson(Map data = [:]) {
        new ProductRequestJson(
                data.get("name", "ProductName"),
                data.get("categories", []),
                data.get("originalPrice", new PriceJson(EUR, BigDecimal.ZERO)),
        )
    }

    static ProductResponseJson productResponseJson(Map data = [:]) {
        ProductResponseJson.builder()
                .name(data.get("name", "ProductName"))
                .productId(data.get("productId", UUID.randomUUID().toString()))
                .categories(data.get("categories", []))
                .originalPrice(data.get("originalPrice", new PriceJson(EUR, BigDecimal.ZERO)))
                .convertedPrice(data.get("convertedPrice", new PriceJson(EUR, BigDecimal.ZERO)))
                .build()
    }

}
