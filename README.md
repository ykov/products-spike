# products-spike

i love docs

## swagger documenation

for swagger documentation visit http://localhost:8080/products-spike/swagger-ui.html

## starting test environment

for starting local test environment

* use 'docker-conpose up' provided in src/test/resources/docker-compose.yml
* mvn clean spring-boot:run -Dspring.profiles.active=local

## ui

ui mapped to http://localhost:3000/products

## using api with curl for product creation :

```bash
curl -X POST \
  'http://localhost:8080/products-spike/products' \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
      	"name": "productName",
      	"categories": ["d8aff8db-83e8-460d-9e9d-e2e2e1a13334", "82d320ce-fc07-47fd-8f08-0386115fb40b"],
      	"originalPrice": {
      		"amount" : 10.99,
      		"currency": "GBP"
      	}
      }

'
```


